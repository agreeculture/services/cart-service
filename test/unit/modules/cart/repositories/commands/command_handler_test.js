

const commandHandler = require('../../../../../../bin/modules/cart/repositories/commands/command_handler');
const domain = require('../../../../../../bin/modules/cart/repositories/commands/domain');
const validator = require('../../../../../../bin/modules/cart/utils/validator');
const wrapper = require('../../../../../../bin/helpers/utils/wrapper');
const EventEmitter = require('events').EventEmitter;
const sinon = require('sinon');
const assert = require('assert');

describe('postOneCart', () => {

  let queryResult = {
    err: null,
    data: {
      id:`1`,
      buyerId:`1`,
      productId:'1',
      qty:'20',
    },
    message: 'Your Request Has Been Processed',
    code: 200
  };

  let payload = {
    id:`1`,
    buyerId:`1`,
    productId:'1',
    qty:'20'
  };

  it('Should return product object', async () => {

    sinon.stub(domain.prototype, 'addNewCart').returns(queryResult);
    sinon.stub(validator, 'ifExistCart').returns(queryResult);

    const rs = await commandHandler.postOneCart(payload);

    assert.equal(rs.err, null);
    assert.equal(rs.code, 200);
    assert.equal(rs.data.qty, '20');

    domain.prototype.addNewCart.restore();
    validator.ifExistCart.restore();

  });
  // it('Should return error event store mockup object', async () => {

  //     let obj = {
  //         err: new Error('whoops!'),
  //         data: null,
  //         message: 'Your Request Can not Processed',
  //         code: 500
  //     };

  //     const spy = sinon.spy();
  //     const emitter = new EventEmitter;

  //     emitter.on('error', spy);
  //     emitter.emit('error')
  //     sinon.stub(domain.prototype, 'addNewMockup').returns(obj);
  //     sinon.stub(domain.prototype, 'publishNewMockup').returns(obj);
  //     sinon.stub(validator, 'ifExistMockup').returns(queryResult);

  //     const rs = await commandHandler.postOneMockup(payload);

  //     // console.log
  //     // assert.equal(rs.err, null);
  //     assert.equal(rs.code, 500);

  //     sinon.assert.calledOnce(spy);

  //     domain.prototype.addNewMockup.restore();
  //     domain.prototype.publishNewMockup.restore();
  //     validator.ifExistMockup.restore();

  // });

});
