'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Cart = require('./domain');

const postOneCart = async (payload) => {
    const product = new Cart();
    const postCommand = async (payload) => {
        return await product.addNewCart(payload);
    }
    return postCommand(payload);
}

const patchOneCart = async (id, payload) => {
    const product = new Cart();
    const putCommand = async (id, payload) => {
        return await product.updateCart(id, payload);
    }
    return putCommand(id, payload);
}

const deleteOneCart = async (id) => {
    const product = new Cart();
    const delCommand = async (id) => {
        return await product.deleteCart(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneCart : postOneCart,
    patchOneCart : patchOneCart,
    deleteOneCart : deleteOneCart
}